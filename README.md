# Quadratic Equation


## General info

This project contains simple gprogram which takes from the user 3 numbers (a, b,c) being respectively coefficients of the quadratic equation. Then it calculates and displays the elements of the quadratic equation (delta, x1, x2).



## Technologies

* JAVA


## Status

Project in progress.
If You have any advice and ideas don't hesitate to contact me.




## Where You can find me

[My LinkedIn profile](linkedin.com/in/patrycja-surmela/)
